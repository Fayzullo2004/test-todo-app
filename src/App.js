import React from "react";
import MyComponent from "./component/My-Component";
import "./style/MySass.scss"
import 'bootstrap/dist/css/bootstrap.min.css';
import Router from "./component/Router";
import {Route, Routes ,BrowserRouter} from "react-router-dom";
import Nav from "./component/Nav";
import Home from "./component/Home";
import "react-transition-group"

function App() {
    return (
        <>
            <div className="App">
                <BrowserRouter>
                <Nav/>
                <Routes>
                    <Route exact path="/" element={<Home/>}/>
                    <Route path="/jadval" element={<MyComponent/>}/>
                    <Route path="/home" element={<Router/>}/>
                    <Route exact path="/" element={<Home/>}/>
                </Routes>
                </BrowserRouter>
            </div>
        </>
    );
}

export default App;
