import React from 'react';

function Selekt({optins,DefoultValue,onChange,value }) {
    return (
        <select className='form-select w-100  '
                value={value}
                onChange={e => onChange( e .target . value )}
        >
            <option disabled value="">{DefoultValue}</option>
            {optins.map(opt => (
                <option key={opt.value} value={opt.value}>
                    {opt.name}
                </option>
            ))}
        </select>
);
}

export default Selekt;
