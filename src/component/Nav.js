import React from 'react';
import {Link} from "react-router-dom";

function Nav(props) {
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <Link to={'/'} className="navbar-brand" href="#">Navbar</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link to={"/home"} className="nav-link" href="#">Home <span className="sr-only"></span></Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/jadval'} className="nav-link" href="#">Jadval</Link>
                        </li>
                        <li className="nav-item">
                            <Link to={'/'} className="nav-link" href="#">Page</Link>
                        </li>
                    </ul>
                </div>
            </nav>

        </>
    );
}

export default Nav;