import React, {useMemo, useState} from 'react';
import Button from "../UI/Button/Button";
import List from "./List";
import Input from "../UI/Input/Input";
import Selekt from "../UI/Select/Selekt";

function MyComponent() {
    const [posts, setposts] = useState([]);
    const [title, setTitle] = useState('')
    const [Stact, setStact] = useState('')

    const [select, setselect] = useState('')
    const [search, setsearch] = useState('')


    const sortedPosts = useMemo(() => {
        if (select) {
            return [...posts].sort((a, b) => a[select].localeCompare(b[select]))
        }
        return posts
    }, [select, posts])

    const sortedAndSearchPosts = useMemo(() => {
        return sortedPosts.filter(post => post.title.toLowerCase().includes(search.toLowerCase()))
    }, [search, sortedPosts])

    const sortPost = (sort) => {
        setselect(sort)
    }

    const addPost = (e) => {
        e.preventDefault()
        const newPost = {
            id: Date.now(),
            title,
            Stact
        }
        setposts([...posts, newPost])
        setTitle('')
        setStact('')
    }

    const removePost = (post) => {
        setposts(posts.filter(s => s.id !== post.id))
    }
    return (
        <div className={""}>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-6 offset-3">
                        <Input
                            type="text"
                            className="form-control my-3"
                            placeholder=" Qayta Royxatan otish"
                            value={title}
                            onChange={e => setTitle(e.target.value)}
                        />
                        <input type="text"
                               className="form-control my-3"
                               placeholder="Royxatan otish"
                               value={Stact}
                               onChange={e => setStact(e.target.value)}
                        />
                        <Button onClick={addPost}>
                            Add Post
                        </Button>
                        <div className="row">
                            <div className="col">
                                <Input
                                    placeholder="Search..."
                                    className="form-control"
                                    value={search}
                                    onChange={e => setsearch(e.target.value)}
                                />
                            </div>
                            <div className="col">
                                <Selekt
                                    value={select}
                                    onChange={sortPost}
                                    DefoultValue="Sorted by"
                                    optins={[
                                        {value: "title", name: "alfabit"},
                                    ]}/>
                            </div>
                        </div>
                        {sortedAndSearchPosts.length
                            ?
                            <List remov={removePost} posts={sortedAndSearchPosts} title=" Favurd Propgraming Language"/>
                            : <h3 className="text-center text-danger my-3">Postlar Qolmadi</h3>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MyComponent;
